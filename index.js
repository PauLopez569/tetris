var c = document.getElementById("canvas");
var ctx = c.getContext("2d");
/*var c2 = document.getElementById("canvas2");
var ctx2 = c2.getContext("2d");*/

var intervalus = "";

var principal = {
    tablero: [],
    estadoJuego: false,
    estadoTablero: false,
    puntuacion: 0,
    puntuacionMax: 0,
    piezaActual: "",
    piezaSiguiente: "",
    contPiezas: {//contador de las piezas de cada tipo
        i:0,
        j:0,
        l:0,
        o:0,
        s:0,
        t:0,
        z:0
    },
    contPiezasGeneral: 0, // contador de todas las piezas
    nivel: 1,
    intervalo:"",

    iniciarJuego: function(){
        this.tablero = this.iniciarTablero();
        var piezaGenerada = this.piezasForma();
        this.piezaActual = "";
        this.estadoJuego = true;
        this.estadoTablero = true;
        this.puntuacion = 0;
        this.puntuacionMax = 0;
        this.piezaSiguiente = new Pieza(piezaGenerada[0],piezaGenerada[1],3,0);
        this.contPiezas = {
            i:0,
            j:0,
            l:0,
            o:0,
            s:0,
            t:0,
            z:0
        };
        this.contPiezasGeneral = 0;
        this.nivel = 1;
        this.intervalo = 1000;
        this.calcularPiezaSiguiente();
    },

    //En esta función se crea el tablero con 25*10
    iniciarTablero: function(){
        var tablero = [];
        for ( var i = 0; i < 25; i++){
            tablero.push([]);
            for(var x = 0; x < 10; x++){
                tablero[i].push(0);
            }
        }
        return tablero;
    },

    piezasForma: function(){ 
        var peces = [[[[0,0,0,0],[0,1,1,0],[0,1,1,0],[0,0,0,0]],"yellow"],
                    [[[0,1,0,0],[0,1,0,0],[0,1,0,0],[0,1,0,0]],"white"],
                    [[[0,0,0,0],[0,1,1,0],[1,1,0,0],[0,0,0,0]],"green"],
                    [[[0,0,0,0],[0,1,1,0],[0,0,1,1],[0,0,0,0]],"red"],
                    [[[0,1,0,0],[0,1,0,0],[0,1,1,0],[0,0,0,0]],"blue"],
                    [[[0,1,1,0],[0,1,0,0],[0,1,0,0],[0,0,0,0]],"orange"],
                    [[[0,0,0,0],[1,1,1,0],[0,1,0,0],[0,0,0,0]],"pink"] ];
                                
        var numeroAleatori = Math.round(Math.random()*6);        
    
        return peces[numeroAleatori];     
    },
    
    //Funcion para calcular la siguiente pieza que aparecera
    calcularPiezaSiguiente: function(){
        var piezaGenerada = this.piezasForma();
        
        this.piezaActual = this.piezaSiguiente;
        this.piezaSiguiente = new Pieza(piezaGenerada[0],piezaGenerada[1],3,0);
    },

    //funcion par detectar el movimiento que quiere el usuario
    movimientoUsuario: function(event){
        switch (event.key) {
            case "ArrowLeft":  // izquierda
                this.piezaActual.moverIzquierda();
                pintarTablero();
                pintarPieza();
                break;
            case "ArrowRight":  // derecha
                this.piezaActual.moverDerecha();
                pintarTablero();
                pintarPieza();
                break;
            case "ArrowDown":  // abajo
                this.piezaActual.moverAbajo();
                pintarTablero();
                pintarPieza();
                    this.puntuar(1);
                break;
            case "z":  // 1 de arriba para rotar a la izquierda
            //console.log("entra");
                this.piezaActual.rotarIzquierda();
                pintarTablero();
                pintarPieza();
                break;
            case "c":  // 3 de arriba para rotar a la derecha
                this.piezaActual.rotarDerecha();
                pintarTablero();
                pintarPieza();
                break;
        }
    },
//funcion para sumar puntos, segun lo que se le pase sumara mas o menos
    puntuar: function(num){
        this.puntuacion += num;
    },

    subirNivel: function(){

        this.contPiezasGeneral += 1;
        if(this.contPiezasGeneral%10 == 0){
            this.nivel += 1;
            clearInterval(intervalus);
            intervalus = getIntervalus();
            this.intervalo = this.intervalo-100;
            this.puntuar(20);
        }
    },
 //funcion de movimiento automático
    movimientoAuto: function(){
        if(this.piezaActual.moverAbajo() == false){
            var forma = this.piezaActual.getFormaPieza();

            for (var i=0; i<forma.length; i++) {
                for (var x=0; x<forma[i].length; x++) {
                    if(forma[i][x] != 0){
                        this.tablero[i+this.piezaActual.y][x+this.piezaActual.x] = this.piezaActual.getColorPieza();
                    }
                }
            }
            pintarPieza();
            this.limpiarFila();
            this.calcularPiezaSiguiente();
            this.subirNivel();
            this.puntuar(10);
        }else{
            pintarPieza();
        }
    },
//Funcion para limpiar una fila que tenga todas las piezas
    limpiarFila: function(){
       var tableroAux = this.tablero;

        for (var i=0; i<tableroAux.length; i++){
            if(!tableroAux[i].includes(0)){ //comprueba si en la fila y del tablero no contiene 0
               tableroAux.splice(i,1);// borra un elemento a partir de la fila i
               tableroAux.unshift(new Array(10).fill(0)); // añade nuevas posiciones al principio del array y rellena con 0
            }
        }
        this.tablero = tableroAux;
        pintarTablero();
       
    }
}

//funcion que pinta el tablero y la informacion del juego en pantalla
function pintarTablero(){
    ctx.clearRect(0, 0, c.width, c.height);
    principal.tablero.forEach((posicion,y) => {
        posicion.forEach((element,x) => {

            switch(element){
                case 0:ctx.fillStyle = "purple";
                       ctx.fillRect(x*35,y*35,35,35);
                       ctx.strokeRect(x*35,y*35,35,35);
                break;
                case "orange":ctx.fillStyle = "orange";
                              ctx.fillRect(x*35,y*35,35,35);
                              ctx.strokeRect(x*35,y*35,35,35);
                break;
                case "yellow":ctx.fillStyle = "yellow";
                              ctx.fillRect(x*35,y*35,35,35);
                              ctx.strokeRect(x*35,y*35,35,35);
                break;
                case "green":ctx.fillStyle = "green";
                             ctx.fillRect(x*35,y*35,35,35);
                             ctx.strokeRect(x*35,y*35,35,35);
                break;
                case "blue":ctx.fillStyle = "blue";
                              ctx.fillRect(x*35,y*35,35,35);
                              ctx.strokeRect(x*35,y*35,35,35);
                break;
                case "white":ctx.fillStyle = "white";
                            ctx.fillRect(x*35,y*35,35,35);
                            ctx.strokeRect(x*35,y*35,35,35);
                break;
                case "pink":ctx.fillStyle = "pink";
                            ctx.fillRect(x*35,y*35,35,35);
                            ctx.strokeRect(x*35,y*35,35,35);
                break;
                case "red":ctx.fillStyle = "red";
                           ctx.fillRect(x*35,y*35,35,35);
                           ctx.strokeRect(x*35,y*35,35,35);
                break;
            }
        });
      
    });
    $("#canvas").css("display", "");
    $("#info").css("display", "");

    $("#puntuacionActual").text(principal.puntuacion);
    $("#record").text(principal.puntuacionMax);
    $("#level").text(principal.nivel);  
}

/*function pintarTablero2(){
    var formaActual = principal.piezaActual.getFormaPieza();
    var posX = principal.piezaActual.getPosicionX();
    var posY = principal.piezaActual.getPosicionY();

    ctx2.clearRect(0, 0, c.width, c.height);
    
    for (var i=0; i < formaActual.length; i++) {
        for (var x=0; x < formaActual[i].length; x++) {
            if (formaActual[i][x] == 1) {
                ctx2.fillStyle = principal.piezaActual.getColorPieza();
                ctx2.fillRect((posX+x)*35,(posY+i)*35,35,35);
                ctx2.strokeRect((posX+x)*35,(posY+i)*35,35,35);
            }
        }
    }
}*/




function pintarPieza(){
    var formaActual = principal.piezaActual.getFormaPieza();
    var posX = principal.piezaActual.getPosicionX();
    var posY = principal.piezaActual.getPosicionY();

    for (var i=0; i < formaActual.length; i++) {
        for (var x=0; x < formaActual[i].length; x++) {
            if (formaActual[i][x] == 1) {
                ctx.fillStyle = principal.piezaActual.getColorPieza();
                ctx.fillRect((posX+x)*35,(posY+i)*35,35,35);
                ctx.strokeRect((posX+x)*35,(posY+i)*35,35,35);
            }
        }
    }
}


principal.iniciarJuego();

function getIntervalus(){
    return setInterval(function () {
        pintarTablero();
        //pintarTablero2();
        principal.movimientoAuto();
    }, principal.intervalo);
}
intervalus = getIntervalus();