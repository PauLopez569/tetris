var Pieza = function(forma, color, x, y){ 
  this.forma = forma;
  this.color = color;
  this.x = x;  
  this.y = y;
};
  
Pieza.prototype.moverDerecha = function(){ 
    var posicionSiguienteY = this.y; //variable para saber si la siguiente posicion estara vacia
    var posicionSiguienteX = this.x + 1;
    var tableroAux = principal.tablero;
    var puedeMover = true;

    for (var i=0; i<this.forma.length; i++) {
        for (var x=0; x<this.forma[i].length; x++) {
            if(this.forma[i][x] != 0){
                if(posicionSiguienteY + i > 24 || tableroAux[posicionSiguienteY + i][posicionSiguienteX + x] != 0){
                    puedeMover = false;
                }
           }
        }
    }

    if(puedeMover){
        this.x = posicionSiguienteX;
    }
    return puedeMover; 
};
   
Pieza.prototype.moverIzquierda = function(){ 
    var posicionSiguienteY = this.y; //variable para saber si la siguiente posicion estara vacia
    var posicionSiguienteX = this.x - 1;
    var tableroAux = principal.tablero;
    var puedeMover = true;

    for (var i=0; i<this.forma.length; i++) {
        for (var x=0; x<this.forma[i].length; x++) {
            if(this.forma[i][x] != 0){
                if(posicionSiguienteY + i > 24 || tableroAux[posicionSiguienteY + i][posicionSiguienteX + x] != 0){
                    puedeMover = false;
                }
           }
        }
    }

    if(puedeMover){
        this.x = posicionSiguienteX;
    }
    return puedeMover; 

};

Pieza.prototype.moverAbajo = function(){ 
    var posicionSiguienteY = this.y + 1; //variable para saber si la siguiente posicion estara vacia
    var posicionSiguienteX = this.x;
    var tableroAux = principal.tablero;
    var puedeMover = true;

    for (var i=0; i<this.forma.length; i++) {
        for (var x=0; x<this.forma[i].length; x++) {
            if(this.forma[i][x] != 0){
                if(posicionSiguienteY + i > 24 || tableroAux[posicionSiguienteY + i][posicionSiguienteX + x] != 0){
                    puedeMover = false;
                }
           }
        }
    }

    if(puedeMover){
        this.y = posicionSiguienteY;
    }
    return puedeMover; 
};  

Pieza.prototype.rotarDerecha = function(){
    var formaNova = new Array();
    var posicionSiguienteY = this.y;
    var posicionSiguienteX = this.x;
    var tableroAux = principal.tablero;
    var puedeRotar = true;

    console.log(this.forma);
    for (var i=0;i<this.forma.length;i++) {
        formaNova[i]=new Array();
        for (var j=0;j<this.forma[i].length;j++) {
            formaNova[i][j]=this.forma[this.forma[i].length-1-j][i];
        }
    }

    for (var i=0; i<formaNova.length; i++) {
        for (var x=0; x < formaNova[i].length; x++) {
            if(formaNova[i][x] != 0){
                if(posicionSiguienteY + i > 24 || tableroAux[posicionSiguienteY + i][posicionSiguienteX + x] != 0 || posicionSiguienteX + x > 10){
                    puedeRotar = false;
                }
           }
        }
    }
    if(puedeRotar){
        this.forma = formaNova;
    }
    console.log(this.forma);
    return puedeRotar; 
};

Pieza.prototype.rotarIzquierda = function(){
    this.rotarDerecha();
    this.rotarDerecha();
    this.rotarDerecha();
};

Pieza.prototype.getFormaPieza = function (){
    return this.forma;
};

Pieza.prototype.getColorPieza = function (){
    return this.color;
};

Pieza.prototype.getPosicionX = function (){
    return this.x;
};

Pieza.prototype.getPosicionY = function (){
    return this.y;
};
